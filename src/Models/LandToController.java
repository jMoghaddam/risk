package Models;

import gui.gameboard.Land;

public interface LandToController {
    void onLandClick(Land clickedLand);
}
