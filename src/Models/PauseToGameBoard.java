package Models;

public interface PauseToGameBoard {
    void unpauseClick();
    void exit();
}
