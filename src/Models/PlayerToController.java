package Models;

import logic.Player;

public interface PlayerToController {
    void winGame(Player player);
}
