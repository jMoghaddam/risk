package Models;

public interface StartGame {
    void onGameStart(int numberOfPlayers);
}
