package Models;

import gui.gameboard.Land;
import logic.Player;

public interface WisemanToController {
    void updateTroopsBoard(Player player, int troopsLeft);
    void wipeTroopsBoard();
    void updateInfoBoard(Player player);
    void updateStateBoard(int stateId);
    void updateAttackMode(Player attacker, Land land);
    void allLandsToNormal();
    void updateDiceBoard();
    void wipeDiceBoard();
    Land getLand(int id);
}
