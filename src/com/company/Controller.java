package com.company;

import Models.*;
import gui.Hero;
import gui.gameboard.GameBoard;
import gui.gameboard.Land;
import helpers.GameRules;
import logic.Player;
import logic.Wiseman;
import java.util.ArrayList;
import java.util.Random;

public class Controller {

    public GameBoard gameboard;
    public Wiseman wiseman;
    private final int numberOfPlayers;
    private final Random random = new Random();
    private ControllerToMainMenu controllerToMainMenu;

    // Controls the interaction between the logic and graphic parts
    public Controller(int numberOfPlayers)  {

        this.numberOfPlayers = numberOfPlayers;

        wiseman = new Wiseman(numberOfPlayers);
        gameboard = new GameBoard(numberOfPlayers);
        gameboard.setVisible(true);

        // Connection between Gameboard and Controller
        gameboard.setGameBoardToController(new GameBoardToController() {
            @Override
            public void onClick() {
                wiseman.clearAction();
            }

            @Override
            public void exitGame() {
                controllerToMainMenu.exit();
            }
        });

        // Connection between Lands and Controller
        for (int id : GameRules.countryIds) {
            gameboard.getMap().getLand(id).setLandToController(new LandToController() {
                @Override
                public void onLandClick(Land clickedLand) {
                    wiseman.landClicked(clickedLand);
                }
            });
        }

        // Connection between Stateboard and Controller
        gameboard.getStateBoard().setStateBoardToController(new StateBoardToController() {
            @Override
            public void skipLevel() {
                wiseman.skipWar();
            }
        });

        // connection between Wiseman and Controller
        wiseman.setWisemanToController(new WisemanToController() {
            @Override
            public void updateTroopsBoard(Player player, int troopsLeft) {
                gameboard.getTroopsBoard().setTurn(player, troopsLeft);
            }

            @Override
            public void wipeTroopsBoard() {
                gameboard.getTroopsBoard().wipe();
            }

            @Override
            public void updateInfoBoard(Player player) {
                gameboard.getInfoBoard().updateTurn(player);
            }

            @Override
            public void updateStateBoard(int stateId) {
                switch (stateId) {
                    case 1:
                        gameboard.getStateBoard().updateToLevel1();
                        break;

                    case 2:
                        gameboard.getStateBoard().updateToLevel2();
                        break;

                    case 3:
                        gameboard.getStateBoard().updateToLevel3();
                        break;
                }
            }

            @Override
            public void updateAttackMode(Player attacker, Land land) {
                int landId = land.id;
                land.becomeRed();

                if (landId - 1 >= 0 && (landId - 1) % 6 != 0) {
                    if (gameboard.getMap().getLand(landId - 1).getOwner() != attacker && gameboard.getMap().getLand(landId - 1).type.equals("country")) {
                        gameboard.getMap().getLand(landId - 1).becomeRed();
                    }
                }

                if (landId + 1 <= 42 && landId % 6 != 0) {
                    if (gameboard.getMap().getLand(landId + 1).getOwner() != attacker && gameboard.getMap().getLand(landId + 1).type.equals("country")) {
                        gameboard.getMap().getLand(landId + 1).becomeRed();
                    }
                }

                if (landId - 6 >= 0) {
                    if (gameboard.getMap().getLand(landId - 6).getOwner() != attacker && gameboard.getMap().getLand(landId - 6).type.equals("country")) {
                        gameboard.getMap().getLand(landId - 6).becomeRed();
                    }
                }

                if (landId + 6 <= 42) {
                    if (gameboard.getMap().getLand(landId + 6).getOwner() != attacker && gameboard.getMap().getLand(landId + 6).type.equals("country")) {
                        gameboard.getMap().getLand(landId + 6).becomeRed();
                    }
                }
            }

            @Override
            public void allLandsToNormal() {
                for (int i = 1; i <= 42; i++) {
                    gameboard.getMap().getLand(i).normalize();
                }
            }

            @Override
            public void updateDiceBoard() {
                gameboard.getDiceBoard().rollDice();
            }

            @Override
            public void wipeDiceBoard() {
                gameboard.getDiceBoard().wipe();
            }

            @Override
            public Land getLand(int id) {
                return gameboard.getMap().getLand(id);
            }
        });

        // Connection between Players and Controller
        for (int i = 1; i <= numberOfPlayers; i++) {
            wiseman.getPlayer(i).setPlayerToController(new PlayerToController() {
                @Override
                public void winGame(Player player) {
                    gameboard.setVisible(false);
                    Hero hero = new Hero(player);
                    hero.setVisible(true);

                    hero.setHeroToController(new HeroToController() {
                        @Override
                        public void exit() {
                            controllerToMainMenu.exit();
                        }
                    });
                }
            });
        }

        // random lands to start the game
        setDefaultMap();

    }

    // randomly assign lands to players to start the game
    public void setDefaultMap() {

        ArrayList<Integer> playerIds = new ArrayList<>();
        int numberOfPlayers = this.numberOfPlayers;
        int minBound = 29 / numberOfPlayers;
        for (int i = 0; i < numberOfPlayers; i++) {
            playerIds.add(i);
        }

        for (int id : GameRules.countryIds) {

            int chance = random.nextInt(numberOfPlayers);
            boolean isSet = false;
            int counter = 0;

            while (!isSet && counter != numberOfPlayers) {
                if (wiseman.getPlayer(chance + 1).getLandsNumber() < minBound) {
                    changeLandOwner(gameboard.getMap().getLand(id), wiseman.getPlayer(chance + 1));
                    wiseman.getPlayer(chance + 1).sendTroops(gameboard.getMap().getLand(id), 1);
                    isSet = true;
                } else {
                    chance = (chance != numberOfPlayers - 1) ? chance + 1 : 0;
                    counter++;
                }
            }

        }

        for (int id : GameRules.countryIds) {
            if (gameboard.getMap().getLand(id).getOwner() == null) {
                int chance = random.nextInt(playerIds.size());
                changeLandOwner(gameboard.getMap().getLand(id), wiseman.getPlayer(chance + 1));
                wiseman.getPlayer(chance + 1).sendTroops(gameboard.getMap().getLand(id), 1);
                playerIds.remove(chance);
            }
        }

        gameboard.getTroopsBoard().setTurn(wiseman.getPlayer(1), wiseman.getPlayer(1).getTroopsLeft());
        wiseman.currentState.setState("send initial troops", wiseman.getPlayer(1));

    }

    // Wiseman to Gameboard -> Change land owner
    public void changeLandOwner(Land land, Player owner) {
        land.updateOwner(owner);
        owner.winLand(land);
    }

    /** Setters **/
    public void setControllerToMainMenu(ControllerToMainMenu listener) {
        this.controllerToMainMenu = listener;
    }
}
