package com.company;

import Models.SplashShut;
import gui.MainMenu;
import gui.SplashScreen;
import helpers.SoundHolder;
import helpers.Utils;

public class Main {

    public static void main(String[]args) {


        SplashScreen splash = new SplashScreen();    // Splash Screen
        MainMenu mainMenu = new MainMenu();          // Main Menu of Game

        // Show Splash Screen
        splash.setVisible(true);

        // Close Splash with User Interactions and Open Main Menu
        splash.setSplashShut(new SplashShut() {
            @Override
            public void onSplashShutClicked() {
                Utils.createSound(SoundHolder.SPLASH).start();

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                splash.setVisible(false);
                mainMenu.setVisible(true);
                splash.dispose();
            }
        });

    }

}
