package gui;

import helpers.Screen;
import javax.swing.*;
import java.awt.*;

// Add background to any JFrame
public class Background extends JPanel {

    private Image background;

    public Background(String path) {
        setLayout(null);
        try
        {
            background = Toolkit.getDefaultToolkit().getImage(path);
        }
        catch (Exception e) {
            System.err.println("Problem loading image: " + path);
        }
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        if (background != null)
            g.drawImage(background, 0,0, Screen.getWidth(), Screen.getHeight(),this);
    }}
