package gui;

import Models.HeroToController;
import helpers.*;
import logic.Player;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Hero extends JFrame {

    private HeroToController heroToController;

    public Hero(Player winner) {

        // Set Background Image
        Background bg = new Background(ImagesHolder.MAIN_MENU_BACKGROUND);
        bg.setBounds(0, 0, Screen.getWidth(), Screen.getHeight());
        add(bg);

        // Add Winner Icon
        ImageIcon winnerIcon = Utils.createIcon(Utils.chooseAvatar(winner.id));
        Image image = winnerIcon.getImage();
        Image newimg = image.getScaledInstance(200, 200, Image.SCALE_SMOOTH);
        winnerIcon = new ImageIcon(newimg);
        JLabel holder = new JLabel(winnerIcon);
        holder.setBounds((Screen.getWidth()/2) - 100, 100, 200, 200);
        bg.add(holder);

        // Add Congrats Text
        JLabel heroText = new JLabel("Conquered the world");
        heroText.setForeground(Color.decode(Utils.chooseColor(winner.id)));
        heroText.setFont(Utils.createFont(FontsHolder.SEGOE_UI_BOLD, 80));
        int width = (int) heroText.getPreferredSize().getWidth();
        int height = (int) heroText.getPreferredSize().getHeight();
        heroText.setBounds(Screen.getWidth()/2 - width / 2, 500, width, height);
        bg.add(heroText);

        // ADd Exit Button
        JLabel exit = new JLabel("Exit");
        exit.setForeground(Color.decode(ColorsHolder.SECONDARY));
        exit.setFont(Utils.createFont(FontsHolder.SEGOE_UI_BOLD, 40));
        int exitWidth = (int) exit.getPreferredSize().getWidth();
        int exitHeight = (int) exit.getPreferredSize().getHeight();
        exit.setBounds(Screen.getWidth()/2 - exitWidth / 2, 700, exitWidth, exitHeight);
        exit.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                heroToController.exit();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
        bg.add(exit);

        // Frame Setup
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setUndecorated(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /** Setters **/
    public void setHeroToController(HeroToController listener) {
        this.heroToController = listener;
    }
}
