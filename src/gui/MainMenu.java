package gui;

import Models.ControllerToMainMenu;
import Models.StartGame;
import com.company.Controller;
import gui.gameboard.Land;
import helpers.ImagesHolder;
import helpers.Screen;
import logic.CurrentState;
import logic.Player;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

public class MainMenu extends JFrame {

    private final MainMenuOptions op = new MainMenuOptions();
    private Controller controller;

    public MainMenu() {

        // Set Background Image
        Background bg = new Background(ImagesHolder.MAIN_MENU_BACKGROUND);
        bg.setBounds(0, 0, Screen.getWidth(), Screen.getHeight());
        add(bg);

        // Options List in Main Menu
        op.setBounds(Screen.getWidth()/2 - 500, 0, 1000, Screen.getHeight());
        bg.add(op);

        // Start a New Game when Play Icon Pressed
        op.setStartGame(new StartGame() {
            @Override
            public void onGameStart(int numberOfPlayers) {
                controller = new Controller(numberOfPlayers);
                setVisible(false);
                op.setVisible(false);

                // Finish the Game
                controller.setControllerToMainMenu(new ControllerToMainMenu() {
                    @Override
                    public void exit() {
                        Player.idCounter = 1;
                        Land.idCounter = 1;
                        controller = null;
                        setVisible(true);
                        op.setVisible(true);
                        CurrentState.reset();
                    }
                });
            }
        });

        // Pause Game when ESC Pressed
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
                KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "Left");
        getRootPane().getActionMap().put("Left", new AbstractAction(){
            public void actionPerformed(ActionEvent e)
            {
                op.moveLeft();
            }
        });

        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
                KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "Right");
        getRootPane().getActionMap().put("Right", new AbstractAction(){
            public void actionPerformed(ActionEvent e)
            {
                op.moveRight();
            }
        });

        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Enter");
        getRootPane().getActionMap().put("Enter", new AbstractAction(){
            public void actionPerformed(ActionEvent e)
            {
                controller = new Controller(op.getNumberOfPlayers());
                setVisible(false);
                op.setVisible(false);

                // Finish the Game
                controller.setControllerToMainMenu(new ControllerToMainMenu() {
                    @Override
                    public void exit() {
                        Player.idCounter = 1;
                        Land.idCounter = 1;
                        controller = null;
                        setVisible(true);
                        op.setVisible(true);
                        CurrentState.reset();
                    }
                });
            }
        });

        // Frame Setup
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setUndecorated(true);

    }

}
