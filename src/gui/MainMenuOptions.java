package gui;

import Models.StartGame;
import helpers.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MainMenuOptions extends JPanel {

    private int numberOfPlayers = 2;
    private final JLabel numOfPlayers2 = new JLabel("2 Players");
    private final JLabel numOfPlayers3 = new JLabel("3 Players");
    private final JLabel numOfPlayers4 = new JLabel("4 Players");
    private StartGame startGame;

    public MainMenuOptions() {

        // Frame Setup
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        setOpaque(false);

        // Title
        JLabel title = new JLabel("Risk");
        title.setFont(Utils.createFont(100));
        title.setForeground(Color.decode(ColorsHolder.SECONDARY));

        // Slang
        JLabel slang = new JLabel("Conquer the World");
        slang.setFont(Utils.createFont(35));
        slang.setForeground(Color.decode(ColorsHolder.PRIMARY));

        // Play Icon
        ImageIcon playIcon = Utils.createIcon(ImagesHolder.MAIN_MENU_PLAY_ICON);
        JLabel playLabel = new JLabel(playIcon);
        playLabel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                startGame();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        // Number of Players Options
        numOfPlayers2.setFont(Utils.createFont(FontsHolder.SEGOE_UI_BOLD, 35));
        numOfPlayers2.setForeground(Color.decode(ColorsHolder.SECONDARY));
        numOfPlayers2.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                selectNumOfPlayers(numOfPlayers2);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        numOfPlayers3.setFont(Utils.createFont(FontsHolder.SEGOE_UI_BOLD, 35));
        numOfPlayers3.setForeground(Color.decode(ColorsHolder.SECONDARY));
        numOfPlayers3.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                selectNumOfPlayers(numOfPlayers3);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        numOfPlayers4.setFont(Utils.createFont(FontsHolder.SEGOE_UI_BOLD, 35));
        numOfPlayers4.setForeground(Color.decode(ColorsHolder.SECONDARY));
        numOfPlayers4.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                selectNumOfPlayers(numOfPlayers4);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        // First Row - Title
        gbc.weightx = 1;
        gbc.weighty = 0.01;
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.NORTH;
        gbc.fill = GridBagConstraints.NONE;
        add(title, gbc);

        // Second Row - Slang
        gbc.weightx = 1;
        gbc.weighty = 0.8;
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.PAGE_START;
        gbc.fill = GridBagConstraints.NONE;
        add(slang, gbc);

        // Third Row - Play Icon
        gbc.weightx = 1;
        gbc.weighty = 0.3;
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.PAGE_START;
        gbc.fill = GridBagConstraints.NONE;
        add(playLabel, gbc);

        // Forth Row - Select the Number of Players Options
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.PAGE_START;
        gbc.fill = GridBagConstraints.NONE;
        add(numOfPlayers2, gbc);
        selectNumOfPlayers(numOfPlayers2);

        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.PAGE_START;
        gbc.fill = GridBagConstraints.NONE;
        add(numOfPlayers3, gbc);

        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridx = 2;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.PAGE_START;
        gbc.fill = GridBagConstraints.NONE;
        add(numOfPlayers4, gbc);

    }

    // Select the Number of Players
    private void selectNumOfPlayers(JLabel numOfPlayers) {
        numOfPlayers.setBorder(BorderFactory.createCompoundBorder(new LineBorder(Color.decode(ColorsHolder.SECONDARY), 5), new EmptyBorder(10, 20, 10, 20)));

        if (numOfPlayers != numOfPlayers2) {
            numOfPlayers2.setBorder(BorderFactory.createCompoundBorder(new LineBorder(new Color(255, 255, 255, 0), 5), new EmptyBorder(10, 20, 10, 20)));
        } else { numberOfPlayers = 2; }

        if (numOfPlayers != numOfPlayers3) {
            numOfPlayers3.setBorder(BorderFactory.createCompoundBorder(new LineBorder(new Color(255, 255, 255, 0), 5), new EmptyBorder(10, 20, 10, 20)));
        } else { numberOfPlayers = 3; }

        if (numOfPlayers != numOfPlayers4) {
            numOfPlayers4.setBorder(BorderFactory.createCompoundBorder(new LineBorder(new Color(255, 255, 255, 0), 5), new EmptyBorder(10, 20, 10, 20)));
        } else { numberOfPlayers = 4; }
    }

    // Move Selected Number of Players with Arrow
    public void moveLeft() {

        switch (numberOfPlayers) {

            case 2:
                selectNumOfPlayers(numOfPlayers4);
                break;

            case 3:
                selectNumOfPlayers(numOfPlayers2);
                break;

            case 4:
                selectNumOfPlayers(numOfPlayers3);

        }

    }

    public void moveRight() {

        switch (numberOfPlayers) {

            case 2:
                selectNumOfPlayers(numOfPlayers3);
                break;

            case 3:
                selectNumOfPlayers(numOfPlayers4);
                break;

            case 4:
                selectNumOfPlayers(numOfPlayers2);

        }

    }

    // Start the Game
    private void startGame() {
        startGame.onGameStart(numberOfPlayers);
    }

    /** Getters **/
    public int getNumberOfPlayers() {
        return numberOfPlayers;
    }

    /** Setters **/
    public void setStartGame(StartGame listener) {
        this.startGame = listener;
    }

}
