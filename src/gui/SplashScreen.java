package gui;

import Models.SplashShut;
import helpers.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Timer;
import java.util.TimerTask;

public class SplashScreen extends JFrame {

    private SplashShut splashShut;    // Interaction between Splash Screen and Main

    public SplashScreen() {

        // Set Background Image
        Background bg = new Background(ImagesHolder.SPLASH_TEXT);
        bg.setBounds(0, 0, Screen.getWidth(), Screen.getHeight());
        add(bg);

        // Set Text
        JLabel splashText = new JLabel("P r e s s   a n y   k e y   t o   c o n t i n u e");
        splashText.setFont(Utils.createFont(FontsHolder.SEGOE_UI_BOLD, 35));
        splashText.setForeground(new Color(110, 102, 88, 0));
        int splashTextWidth = (int) splashText.getPreferredSize().getWidth();
        int splashTextHeight = (int) splashText.getPreferredSize().getHeight();
        splashText.setBounds(Screen.getWidth()/2 - splashTextWidth / 2, Screen.getHeight()/2 + 150, splashTextWidth, splashTextHeight);
        bg.add(splashText);

        // Close Splash when any Key Pressed
        addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                System.out.println("hi");
                splashShut.onSplashShutClicked();
            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        // Close Splash when Mouse Button pressed
        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                splashShut.onSplashShutClicked();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        final int[] i = {0};
        final boolean[] isIncreasing = {true};
        java.util.Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                splashText.setForeground(new Color(110, 102, 88, i[0]));
                if (isIncreasing[0]) {
                    if (i[0] != 255) i[0]++;
                    else {
                        isIncreasing[0] = false;
                        try {
                            Thread.sleep(250);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    if (i[0] != 0) i[0]--;
                    else isIncreasing[0] = true;
                }
            }
        }, 500, 5);

        // Frame Setup
        setLayout(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setUndecorated(true);

    }

    /** Setters **/
    public void setSplashShut(SplashShut splashShut) {
        this.splashShut = splashShut;
    }

}
