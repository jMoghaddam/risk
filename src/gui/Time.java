package gui;

public class Time {

    private static int hour = 0;
    private static int min = 0;
    private static int sec = 0;

    public static void addSec() {

        if ( sec + 1 < 60 ) {
            sec++;
        } else if (min < 60) {
            min++;
            sec = 0;
        } else {
            hour++;
            min = 0;
            sec = 0;
        }

    }

    /** Getters **/
    public static int getHour() {
        return hour;
    }

    public static int getMin() {
        return min;
    }

    public static int getSec() {
        return sec;
    }
}
