package gui.gameboard;

import helpers.ImagesHolder;
import helpers.Utils;
import logic.CurrentState;
import logic.Player;
import javax.swing.*;
import java.awt.*;

public class DiceBoard extends JPanel {

    private final JPanel container = new JPanel();
    private final ImageIcon[] dices = new ImageIcon[6];
    private final ImageIcon[] winners = new ImageIcon[3];
    private Player attacker;
    private Player defender;

    public DiceBoard() {

        // Frame Setup
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        container.setLayout(new GridLayout(3, 3, 15, 15));
        for (int i = 0; i < 9; i++) {
            container.add(new JLabel(""));
        }

        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.CENTER;
        add(container, gbc);

        container.setBorder(BorderFactory.createEmptyBorder(115, 115, 115, 115));

    }

    /** Graphic Updaters **/
    public void rollDice() {

        container.removeAll();
        container.setBorder(null);

        this.attacker = CurrentState.attacker.getOwner();
        this.defender = CurrentState.defender.getOwner();

        for (int i = 0; i < 3; i++) {
            if (i < CurrentState.attackers.size()) {
                dices[i] = Utils.createIcon(chooseDice(CurrentState.attackers.get(i)));
                Image image = dices[i].getImage();
                Image newimg = image.getScaledInstance(75, 75, Image.SCALE_SMOOTH);
                dices[i] = new ImageIcon(newimg);
                JLabel holder = new JLabel(dices[i]);
                container.add(holder);
            } else {
                container.add(new JLabel(""));
            }
        }

        for (int i = 0; i < 3; i++) {
            if (i < CurrentState.defenders.size()) {
                dices[i + CurrentState.attackers.size()] = Utils.createIcon(chooseDice(CurrentState.defenders.get(i)));
                Image image = dices[i + CurrentState.attackers.size()].getImage();
                Image newimg = image.getScaledInstance(75, 75, Image.SCALE_SMOOTH);
                dices[i + CurrentState.attackers.size()] = new ImageIcon(newimg);
                JLabel holder = new JLabel(dices[i + CurrentState.attackers.size()]);
                container.add(holder);
            } else {
                container.add(new JLabel(""));
            }
        }

        for (int i = 0; i < Math.min(CurrentState.attackers.size(), CurrentState.defenders.size()); i++) {

            winners[i] = Utils.createIcon(chooseWinner(i));
            Image image = winners[i].getImage();
            Image newimg = image.getScaledInstance(75, 75, Image.SCALE_SMOOTH);
            winners[i] = new ImageIcon(newimg);
            JLabel holder = new JLabel(winners[i]);
            container.add(holder);

        }

    }

    public void wipe() {
        container.removeAll();
        container.setBorder(BorderFactory.createEmptyBorder(115, 115, 115, 115));

        for (int i = 0; i < 9; i++) {
            container.add(new JLabel(""));
        }
    }

    private String chooseDice(int number) {
        switch (number) {
            case 1:
                return ImagesHolder.DICE1;

            case 2:
                return ImagesHolder.DICE2;

            case 3:
                return ImagesHolder.DICE3;

            case 4:
                return ImagesHolder.DICE4;

            case 5:
                return ImagesHolder.DICE5;

            case 6:
                return ImagesHolder.DICE6;

            default:
                return "";
        }
    }

    private String chooseWinner(int index) {
        int playerId;

        if (CurrentState.attackers.get(index) > CurrentState.defenders.get(index)) {
            playerId = attacker.id;
        } else {
            playerId = defender.id;
        }

        switch (playerId) {
            case 1:
                return ImagesHolder.PLAYER1;

            case 2:
                return ImagesHolder.PLAYER2;

            case 3:
                return ImagesHolder.PLAYER3;

            case 4:
                return ImagesHolder.PLAYER4;

            default:
                return "";
        }
    }

}
