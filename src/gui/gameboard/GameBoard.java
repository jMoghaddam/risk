package gui.gameboard;

import Models.GameBoardToController;
import Models.PauseToGameBoard;
import helpers.Screen;
import logic.CurrentState;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class GameBoard extends JFrame {

    private final JPanel gamePanel = new JPanel();
    private final Pause pausePanel = new Pause();
    private final Map map;
    private final PlayersList playersList;
    private final InfoBoard infoBoard;
    private final DiceBoard diceBoard;
    private final StateBoard stateBoard;
    private final TroopsBoard troopsBoard;
    private GameBoardToController gameBoardToController;

    public GameBoard(int numOfPlayers) {

        // Frame Setup
        gamePanel.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                gameBoardToController.onClick();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        playersList = new PlayersList(numOfPlayers);
        map = new Map();
        infoBoard = new InfoBoard();
        diceBoard = new DiceBoard();
        stateBoard = new StateBoard();
        troopsBoard = new TroopsBoard();

        // Players List Board
        gbc.weightx = 0.4;
        gbc.weighty = 0.75;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridheight = 2;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.BOTH;
        gamePanel.add(playersList, gbc);

        // Map Board
        gbc.weightx = (Screen.getWidth() == 1920) ? 1 : 0.5;
        gbc.weighty = 0.75;
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.gridheight = 2;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.BOTH;
        gamePanel.add(map, gbc);

        // Info Board
        gbc.weightx = (Screen.getWidth() == 1920) ? 0.5 : 0.2;
        gbc.weighty = 0.5;
        gbc.gridx = 2;
        gbc.gridy = 0;
        gbc.gridheight = 1;
        gbc.anchor = GridBagConstraints.SOUTH;
        gbc.fill = GridBagConstraints.BOTH;
        gamePanel.add(infoBoard, gbc);

        // Dice Board
        gbc.weightx = 0.2;
        gbc.weighty = 0.45;
        gbc.gridx = 2;
        gbc.gridy = 1;
        gbc.gridheight = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.BOTH;
        gamePanel.add(diceBoard, gbc);

        // Placeholder
        gbc.weightx = 1;
        gbc.weighty = 0.45;
        gbc.gridx = 0;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.BOTH;
        gamePanel.add(new JLabel(""), gbc);

        // State Board
        gbc.weightx = 0.5;
        gbc.weighty = 0.45;
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.BOTH;
        gamePanel.add(stateBoard, gbc);

        // Remaining Troops Board
        gbc.weightx = 0.25;
        gbc.weighty = 0.45;
        gbc.gridx = 2;
        gbc.gridy = 3;
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.BOTH;
        gamePanel.add(troopsBoard, gbc);

        add(gamePanel);

        // Pause Game when ESC Pressed
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(
                KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "Cancel");
        getRootPane().getActionMap().put("Cancel", new AbstractAction(){
            public void actionPerformed(ActionEvent e)
            {
                if (!CurrentState.isPaused) { pause(); }
                else { unpause(); }
            }
        });

        // Pause Interactions
        pausePanel.setPauseToGameBoard(new PauseToGameBoard() {
            @Override
            public void unpauseClick() { unpause(); }

            @Override
            public void exit() {
                gameBoardToController.exitGame();
            }
        });

        // Frame Setup
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setUndecorated(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    // Pause the Game
    private void pause() {
        remove(gamePanel);
        add(pausePanel);
        CurrentState.isPaused = true;
        repaint();
        revalidate();
    }

    // Unpause the Game
    private void unpause() {
        remove(pausePanel);
        add(gamePanel);
        CurrentState.isPaused = false;
        repaint();
        revalidate();
    }

    /** Getters **/
    public Map getMap() {
        return map;
    }

    public PlayersList getPlayersList() {
        return playersList;
    }

    public InfoBoard getInfoBoard() {
        return infoBoard;
    }

    public DiceBoard getDiceBoard() {
        return diceBoard;
    }

    public StateBoard getStateBoard() {
        return stateBoard;
    }

    public TroopsBoard getTroopsBoard() {
        return troopsBoard;
    }

    /** Setters **/
    public void setGameBoardToController(GameBoardToController gameBoardToController) {
        this.gameBoardToController = gameBoardToController;
    }
}
