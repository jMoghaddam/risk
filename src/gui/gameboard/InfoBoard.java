package gui.gameboard;

import gui.Time;
import helpers.Screen;
import helpers.Utils;
import logic.Player;
import javax.swing.*;
import java.awt.*;
import java.util.Timer;
import java.util.TimerTask;

public class InfoBoard extends JLabel {

    private final JLabel text = new JLabel();
    private JLabel timeFormat = new JLabel("0:0:0");

    public InfoBoard() {
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        text.setText("Turn: Player 1");
        int fontSize = (Screen.getWidth() == 1920) ? 30 : 20;
        text.setFont(Utils.createFont(fontSize));
//        text.setBorder(BorderFactory.createCompoundBorder(new LineBorder(Color.decode(ColorsHolder.PRIMARY), 5), new EmptyBorder(75, 100, 75, 100)));

        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.NORTH;
        add(text, gbc);

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                updateTime();
            }
        }, 0, 1000);

        timeFormat.setFont(Utils.createFont(35));
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.SOUTH;
        add(timeFormat, gbc);

    }

    /** Graphic Updaters **/
    public void updateTurn(Player player) {
        text.setText("Turn: Player " + player.id);
    }

    public void updateTime() {
        Time.addSec();
        timeFormat.setText(Time.getHour() + ":" + Time.getMin() + ":" + Time.getSec());
    }

}
