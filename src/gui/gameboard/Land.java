package gui.gameboard;

import Models.LandToController;
import helpers.ColorsHolder;
import helpers.Utils;
import logic.Player;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Land extends JButton {

    public static int idCounter = 1;
    public final int id;
    public final String type;
    private Player owner = null;
    private int soldiers;
    private LandToController landToController;
    public boolean isSearched;

    public Land(String type) {

        this.id = idCounter++;
        this.type = type;

        setBorders();

        setFont(Utils.createFont(45));
        setFocusPainted(false);

        if (type.equals("country")) {
            setText(String.valueOf(soldiers));
        } else {
            setBackground(Color.BLUE);
            setBorder(BorderFactory.createLineBorder(Color.BLUE));
            setEnabled(false);
        }

        // connection between land and controller
        addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                landToController.onLandClick(Land.this);
            }
        });

    }

    private void setBorders() {

        switch (id) {

            case 1:
                setBorder(BorderFactory.createMatteBorder(5, 5, 0, 0, Color.RED));
                break;

            case 2:
                setBorder(BorderFactory.createMatteBorder(5, 0, 0, 5, Color.RED));
                break;

            case 7:
                setBorder(BorderFactory.createMatteBorder(0, 5, 0, 0, Color.RED));
                break;

            case 8:
                setBorder(BorderFactory.createMatteBorder(0, 0, 5, 5, Color.RED));
                break;

            case 13:
            case 19:
            case 25:
                setBorder(BorderFactory.createMatteBorder(0, 5, 0, 5, Color.RED));
                break;

            case 31:
                setBorder(BorderFactory.createMatteBorder(0, 5, 5, 0, Color.RED));
                break;

            case 32:
                setBorder(BorderFactory.createMatteBorder(5, 0, 5, 5, Color.RED));
                break;

            case 3:
                setBorder(BorderFactory.createMatteBorder(5, 5, 0, 0, Color.GREEN));
                break;

            case 4:
                setBorder(BorderFactory.createMatteBorder(5, 0, 0, 5, Color.GREEN));
                break;

            case 9:
                setBorder(BorderFactory.createMatteBorder(0, 5, 0, 0, Color.GREEN));
                break;

            case 10:
                setBorder(BorderFactory.createMatteBorder(0, 0, 0, 5, Color.GREEN));
                break;

            case 15:
                setBorder(BorderFactory.createMatteBorder(0, 5, 5, 0, Color.GREEN));
                break;

            case 16:
                setBorder(BorderFactory.createMatteBorder(0, 0, 5, 5, Color.GREEN));
                break;

            case 5:
                setBorder(BorderFactory.createMatteBorder(5, 5, 0, 0, Color.YELLOW));
                break;

            case 6:
                setBorder(BorderFactory.createMatteBorder(5, 0, 0, 5, Color.YELLOW));
                break;

            case 11:
            case 17:
                setBorder(BorderFactory.createMatteBorder(0, 5, 0, 0, Color.YELLOW));
                break;

            case 12:
            case 18:
                setBorder(BorderFactory.createMatteBorder(0, 0, 0, 5, Color.YELLOW));
                break;

            case 23:
                setBorder(BorderFactory.createMatteBorder(0, 5, 5, 0, Color.YELLOW));
                break;

            case 24:
                setBorder(BorderFactory.createMatteBorder(0, 0, 5, 5, Color.YELLOW));
                break;

            case 22:
                setBorder(BorderFactory.createMatteBorder(5, 5, 0, 5, Color.BLACK));
                break;

            case 27:
                setBorder(BorderFactory.createMatteBorder(5, 5, 0, 0, Color.BLACK));
                break;

            case 28:
                setBorder(BorderFactory.createMatteBorder(0, 0, 0, 5, Color.BLACK));
                break;

            case 33:
                setBorder(BorderFactory.createMatteBorder(0, 5, 0, 0, Color.BLACK));
                break;

            case 34:
                setBorder(BorderFactory.createMatteBorder(0, 0, 5, 5, Color.BLACK));
                break;

            case 39:
                setBorder(BorderFactory.createMatteBorder(0, 5, 5, 5, Color.BLACK));
                break;

        }

    }

    /** Graphic Updaters **/
    public void updateOwner(Player owner) {

        if (type.equals("country")) {
            this.owner = owner;

            switch (owner.id) {

                case 1:
                    setBackground(Color.decode(ColorsHolder.PLAYER1));
                    break;

                case 2:
                    setBackground(Color.decode(ColorsHolder.PLAYER2));
                    break;

                case 3:
                    setBackground(Color.decode(ColorsHolder.PLAYER3));
                    break;

                case 4:
                    setBackground(Color.decode(ColorsHolder.PLAYER4));
                    break;

                default:
                    setEnabled(false);
                    setBackground(Color.cyan);
            }
        }

    }

    public void updateSoldiers(int soldiers) {
        if (type.equals("country")) {
            this.soldiers = soldiers;
            setText(String.valueOf(soldiers));
        }
    }

    public void addSoldier() {
        this.soldiers++;
        setText(String.valueOf(soldiers));
    }

    public void removeSoldier() {
        this.soldiers--;
        setText(String.valueOf(soldiers));
    }

    public void becomeRed() {
        setBorder(BorderFactory.createLineBorder(Color.RED, 5));
    }

    public void normalize() {
        if (type.equals("country")) {
            setBorders();
        }
    }

    /** Getters **/
    public Player getOwner() {
        return owner;
    }

    public int getSoldiers() {
        return soldiers;
    }

    /** Setters **/
    public void setLandToController(LandToController listener) {
        this.landToController = listener;
    }

}
