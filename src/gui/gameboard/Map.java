package gui.gameboard;

import helpers.Utils;
import javax.swing.*;
import java.awt.*;

public class Map extends JLabel {

    private final Land[] lands = new Land[42];

    public Map() {

        // Initialize the lands
        for (int i = 0; i < 42; i++) {
            String type = (Utils.isSea(i+1)) ? "sea" : "country";
            lands[i] = new Land(type);
        }

        // grid layout containing the 42 lands
        setLayout(new GridLayout(7, 6));
        for (int i = 0; i < 42; i++) {
            add(lands[i]);
        }

    }

    /** Getters **/
    public Land getLand(int id) {
        return lands[id - 1];
    }
}
