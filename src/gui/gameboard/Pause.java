package gui.gameboard;

import Models.PauseToGameBoard;
import gui.Background;
import helpers.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Pause extends JLabel {

    private PauseToGameBoard pauseToGameBoard;

    public Pause() {

        // Set Background Image
        Background bg = new Background(ImagesHolder.MAIN_MENU_BACKGROUND);
        bg.setBounds(0, 0, Screen.getWidth(), Screen.getHeight());
        bg.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        add(bg);

        // Resume Text
        JLabel resume = new JLabel("Resume");
        resume.setFont(Utils.createFont(FontsHolder.SEGOE_UI_BOLD, 50));
        resume.setForeground(Color.decode(ColorsHolder.SECONDARY));
        resume.setBorder(BorderFactory.createCompoundBorder(new LineBorder(Color.decode(ColorsHolder.SECONDARY), 10), new EmptyBorder(25, 100, 25, 100)));
        resume.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                pauseToGameBoard.unpauseClick();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        // Exit Text
        JLabel exit = new JLabel("Exit");
        exit.setFont(Utils.createFont(FontsHolder.SEGOE_UI_BOLD, 50));
        exit.setForeground(Color.decode(ColorsHolder.SECONDARY));
        exit.setBorder(BorderFactory.createEmptyBorder(50, 100, 50, 100));
        exit.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                pauseToGameBoard.exit();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        gbc.weighty = 1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.SOUTH;
        bg.add(resume, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.anchor = GridBagConstraints.NORTH;
        bg.add(exit, gbc);

        setLayout(null);

    }

    /** Setters **/
    public void setPauseToGameBoard(PauseToGameBoard listener) {
        this.pauseToGameBoard = listener;
    }

}
