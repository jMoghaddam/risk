package gui.gameboard;

import helpers.Utils;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.*;

public class PlayerCard extends JLabel {

    public PlayerCard(int counter) {

        setText("Player " + (counter + 1));
        setFont(Utils.createFont(30));

        Color playerColor = Color.decode(Utils.chooseColor(counter + 1));
        setBorder(BorderFactory.createCompoundBorder(new LineBorder(playerColor, 5), new EmptyBorder(10, 20, 10, 20)));

        ImageIcon playIcon = Utils.createIcon(Utils.chooseAvatar(counter + 1));
        Image image = playIcon.getImage();
        Image newimg = image.getScaledInstance(120, 120, Image.SCALE_SMOOTH);
        playIcon = new ImageIcon(newimg);
        setIcon(playIcon);

    }

}
