package gui.gameboard;

import javax.swing.*;
import java.awt.*;

public class PlayersList extends JLabel {

    public PlayersList(int numberOfPlayers) {

        // Frame Setup
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        PlayerCard[] playerCards = new PlayerCard[numberOfPlayers];

        for (int i = 0; i < numberOfPlayers; i++) {

            playerCards[i] = new PlayerCard(i);
            gbc.weightx = 1;
            gbc.weighty = 0.25;
            gbc.gridx = 0;
            gbc.gridy = i;
            gbc.anchor = GridBagConstraints.CENTER;
            add(playerCards[i], gbc);

        }

        for (int i = 4; i > 4 - numberOfPlayers; i--) {

            gbc.weightx = 1;
            gbc.weighty = 0.25;
            gbc.gridx = 0;
            gbc.gridy = i;
            gbc.anchor = GridBagConstraints.CENTER;
            gbc.fill = GridBagConstraints.BOTH;
            add(new JLabel(""), gbc);

        }

    }

}
