package gui.gameboard;

import Models.StateBoardToController;
import helpers.ImagesHolder;
import helpers.Utils;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class StateBoard extends JPanel {

    private final JPanel container = new JPanel();
    private final ImageIcon[] states = new ImageIcon[3];
    private final JLabel text = new JLabel("Troops are ready to be deployed");
    private StateBoardToController stateBoardToController;

    public StateBoard() {

        // Frame Setup
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        states[0] = Utils.createIcon(ImagesHolder.CIRCLE_LINE);
        states[1] = Utils.createIcon(ImagesHolder.CIRCLE_LINE);
        states[2] = Utils.createIcon(ImagesHolder.CIRCLE_LINE);

        text.setFont(Utils.createFont(35));
        text.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                stateBoardToController.skipLevel();
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });

        container.setLayout(new GridLayout(1, 3, 170, 0));
        for (int i = 0; i < 3; i++) {
            Image image = states[i].getImage();
            Image newimg = image.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
            states[i] = new ImageIcon(newimg);
            JLabel holder = new JLabel(states[i]);
            container.add(holder);
        }

        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.anchor = GridBagConstraints.SOUTH;
        add(container, gbc);

        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.anchor = GridBagConstraints.CENTER;
        add(text, gbc);

    }

    /** Graphic Updaters **/
    public void updateToLevel1() {

        text.setText("  Troops are ready to be deployed");
        text.setIcon(null);
        states[0] = Utils.createIcon("/images/stateFill.png");
        states[1] = Utils.createIcon("/images/stateLine.png");
        states[2] = Utils.createIcon("/images/stateLine.png");
        updateGrid();

    }

    public void updateToLevel2() {

        text.setText("  Troops are ready for battle    ");

        ImageIcon skip = Utils.createIcon(ImagesHolder.SKIP);
        Image image = skip.getImage();
        Image newimg = image.getScaledInstance(45, 45, Image.SCALE_SMOOTH);
        skip = new ImageIcon(newimg);
        text.setIcon(skip);

        states[0] = Utils.createIcon("/images/stateLine.png");
        states[1] = Utils.createIcon("/images/stateFill.png");
        states[2] = Utils.createIcon("/images/stateLine.png");
        updateGrid();

    }

    public void updateToLevel3() {

        text.setText("  Troops are ready to move");
        states[0] = Utils.createIcon("/images/stateLine.png");
        states[1] = Utils.createIcon("/images/stateLine.png");
        states[2] = Utils.createIcon("/images/stateFill.png");
        updateGrid();

    }

    private void updateGrid() {
        container.removeAll();
        container.setLayout(new GridLayout(1, 3, 170, 0));
        for (int i = 0; i < 3; i++) {
            Image image = states[i].getImage();
            Image newimg = image.getScaledInstance(60, 60, Image.SCALE_SMOOTH);
            states[i] = new ImageIcon(newimg);
            JLabel holder = new JLabel(states[i]);
            container.add(holder);
        }
    }

    /** Setters **/
    public void setStateBoardToController(StateBoardToController listener) {
        stateBoardToController = listener;
    }

}
