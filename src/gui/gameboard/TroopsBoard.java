package gui.gameboard;

import helpers.Utils;
import logic.CurrentState;
import logic.Player;
import javax.swing.*;
import java.awt.*;

public class TroopsBoard extends JLabel {

    public TroopsBoard() { setFont(Utils.createFont(25)); }

    /** Graphic Updaters **/
    public void setTurn(Player player, int troopsLeft) {

        setText(troopsLeft + " Troops");

        ImageIcon avatar = Utils.createIcon(Utils.chooseAvatar(player.id));
        Image image = avatar.getImage();
        Image newimg = image.getScaledInstance(120, 120, Image.SCALE_SMOOTH);
        avatar = new ImageIcon(newimg);
        setIcon(avatar);

    }

    public void wipe() {
        if (CurrentState.state.equals("level2")) { setText("War Mode "); }
        else { setText("Transform"); }
    }

}
