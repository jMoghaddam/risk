package helpers;

public class ColorsHolder {
    public static final String PRIMARY = "#6E6658";
    public static final String SECONDARY = "#4F4A41";
    public static final String PLAYER1 = "#487C2C";
    public static final String PLAYER2 = "#2b2b2b";
    public static final String PLAYER3 = "#CE7D51";
    public static final String PLAYER4 = "#DD3E3F";
}
