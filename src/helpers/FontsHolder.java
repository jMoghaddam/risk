package helpers;

public class FontsHolder {
    public static final String SEGOE_UI_REGULAR = "/fonts/Segoe UI.ttf";
    public static final String SEGOE_UI_BOLD = "/fonts/Segoe UI Bold.ttf";
}
