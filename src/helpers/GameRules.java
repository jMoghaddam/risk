package helpers;

public class GameRules {
    public static final int[] countryIds = {
            1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17,
            18, 19, 22, 23, 24, 25, 27, 28, 31, 32, 33, 34, 39
    };
    public static final int[] seaIds = {14, 20, 21, 26, 29, 30, 35, 36, 37, 38, 40, 41, 42};
    public static final  int[] americaIds = {1, 2, 7, 8, 13, 19, 25, 31, 32};
    public static final  int[] europeIds = {3, 4, 9, 10, 15, 16};
    public static final  int[] asiaIds = {5, 6, 11, 12, 17, 18, 23, 24};
    public static final  int[] africaIds = {22, 27, 28, 33, 34, 39};
}
