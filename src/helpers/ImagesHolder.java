package helpers;

public class ImagesHolder {
    public final static String SPLASH_TEXT = "src/images/Splash.jpg";
    public final static String MAIN_MENU_BACKGROUND = "src/images/Background.jpg";
    public final static String MAIN_MENU_PLAY_ICON = "/images/play.png";
    public final static String PLAYER1_AVATAR = "/images/avatar1.png";
    public final static String PLAYER2_AVATAR = "/images/avatar2.png";
    public final static String PLAYER3_AVATAR = "/images/avatar3.png";
    public final static String PLAYER4_AVATAR = "/images/avatar4.png";
    public final static String DICE1 = "/images/dice1.png";
    public final static String DICE2 = "/images/dice2.png";
    public final static String DICE3 = "/images/dice3.png";
    public final static String DICE4 = "/images/dice4.png";
    public final static String DICE5 = "/images/dice5.png";
    public final static String DICE6 = "/images/dice6.png";
    public final static String PLAYER1 = "/images/player1.png";
    public final static String PLAYER2 = "/images/player2.png";
    public final static String PLAYER3 = "/images/player3.png";
    public final static String PLAYER4 = "/images/player4.png";
    public final static String SKIP = "/images/skip.png";
    public final static String CIRCLE_LINE = "/images/stateLine.png";
    public final static String CIRCLE_FILL = "/images/stateFill.png";
}
