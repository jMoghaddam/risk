package helpers;

import java.awt.*;

public class Screen {
    private static Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private static int width = (int) screenSize.getWidth();
    private static int height = (int) screenSize.getHeight();

    public static int getWidth() {
        return width;
    }

    public static int getHeight() {
        return height;
    }
}
