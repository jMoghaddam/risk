package helpers;

public class SoundHolder {
    public static String SPLASH = "sounds/Splash.wav";
    public static String ALERT = "sounds/Alert.wav";
    public static String WIN = "sounds/win.wav";
}
