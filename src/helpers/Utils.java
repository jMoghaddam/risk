package helpers;

import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.net.URL;

public class Utils {

    // Safely Create Font
    public static Font createFont(String path, int fontSize) {
        URL url = System.class.getResource(path);

        if (url == null) {
            System.err.println("Unable to load font: " + path);
        }

        Font font = null;
        try {
            font = Font.createFont(Font.TRUETYPE_FONT, url.openStream()).deriveFont(Font.PLAIN, fontSize);
        } catch (FontFormatException e) {
            System.err.println("Bad format in file: " + path);
        } catch (IOException e) {
            System.err.println("Unable to read font file: " + path);
        }

        return font;
    }

    public static Font createFont(int fontSize) {
        return createFont(FontsHolder.SEGOE_UI_REGULAR, fontSize);
    }

    // Safely Create Image Icon
    public static ImageIcon createIcon(String path) {
        URL url = System.class.getResource(path);

        if (url == null) {
            System.err.println("Unable to load font: " + path);
        }

        ImageIcon icon = new ImageIcon(url);

        return icon;
    }

    // Safely Create Sound Effect
    public static Clip createSound(String path) {
        try {
            // Open an audio input stream.
            URL url = Utils.class.getClassLoader().getResource(path);
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(url);
            // Get a sound clip resource.
            Clip clip = AudioSystem.getClip();
            // Open audio clip and load samples from the audio input stream.
            clip.open(audioIn);
            return clip;
        } catch (UnsupportedAudioFileException | IOException | LineUnavailableException e) {
            e.printStackTrace();
        }

        return null;
    }

    // Check if Land is Sea
    public static boolean isSea(int targetValue) {
        for (int seaId : GameRules.seaIds) {
            if (seaId == targetValue) return true;
        }
        return false;
    }

    // Return the Color of Player
    public static String chooseColor(int id) {
        switch (id) {
            case 1:
                return ColorsHolder.PLAYER1;

            case 2:
                return ColorsHolder.PLAYER2;

            case 3:
                return ColorsHolder.PLAYER3;

            case 4:
                return ColorsHolder.PLAYER4;

            default:
                return "";
        }
    }

    // Return the Avatar of Player
    public static String chooseAvatar(int id) {
        switch (id) {
            case 1:
                return ImagesHolder.PLAYER1_AVATAR;

            case 2:
                return ImagesHolder.PLAYER2_AVATAR;

            case 3:
                return ImagesHolder.PLAYER3_AVATAR;

            case 4:
                return ImagesHolder.PLAYER4_AVATAR;

            default:
                return "";
        }
    }

    public static boolean contains(final int[] array, final int v) {

        boolean result = false;

        for(int i : array){
            if(i == v){
                result = true;
                break;
            }
        }

        return result;
    }

}
