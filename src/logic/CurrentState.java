package logic;

import gui.gameboard.Land;
import java.util.ArrayList;

public class CurrentState {

    public static String state;
    public static Player who;
    public static Land attacker;
    public static Land defender;
    public static ArrayList<Integer> attackers = new ArrayList<>();
    public static ArrayList<Integer> defenders = new ArrayList<>();
    public static Land origin;
    public static Land destination;
    public static boolean isPaused = false;

    public void setState(String state, Player who) {
        CurrentState.state = state;
        CurrentState.who = who;
    }

    public static void reset() {
        state = null;
        who = null;
        attacker = null;
        defender = null;
        attackers.clear();
        defenders.clear();
        origin = null;
        destination = null;
        isPaused = false;
    }

}
