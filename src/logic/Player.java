package logic;

import Models.PlayerToController;
import gui.gameboard.Land;
import helpers.GameRules;
import helpers.Utils;
import java.util.ArrayList;

public class Player {

    public static int idCounter = 1;
    public final int id;
    private final ArrayList<Land> lands = new ArrayList<>();
    private final ArrayList<String> continents = new ArrayList<>();
    private int troopsLeft;
    private PlayerToController playerToController;

    public Player(int initialTroops) {
        id = idCounter++;
        this.troopsLeft = initialTroops;
    }

    // Add Land
    public void winLand(Land land) {
        lands.add(land);
        checkContinents();
        checkWin();
    }

    // Remove Land
    public void looseLand(Land land) {
        lands.remove(land);
        checkContinents();
    }

    // Send Troops
    public void sendTroops(Land destination, int howMany) {

        if (howMany <= troopsLeft) {
            destination.updateSoldiers(howMany + destination.getSoldiers());
            troopsLeft = troopsLeft - howMany;
        }

    }

    // Check Won Continents
    private void checkContinents() {

        int americas = 0, europes = 0, asias = 0, africas = 0;

        for (Land land : lands) {
            if (Utils.contains(GameRules.americaIds, land.id)) {
                americas++;
            }

            if (Utils.contains(GameRules.europeIds, land.id)) {
                europes++;
            }

            if (Utils.contains(GameRules.asiaIds, land.id)) {
                asias++;
            }

            if (Utils.contains(GameRules.africaIds, land.id)) {
                africas++;
            }
        }

        if (americas == GameRules.americaIds.length) {
            continents.add("america");
        }

        if (europes == GameRules.europeIds.length) {
            continents.add("europe");
        }

        if (asias == GameRules.asiaIds.length) {
            continents.add("asia");
        }

        if (africas == GameRules.africaIds.length) {
            continents.add("africa");
        }

    }

    // Check if Player has All the Lands
    private void checkWin() {
        if (lands.size() == 29) {
            playerToController.winGame(this);
        }
    }

    /** Getters **/
    public int getLandsNumber() {
        return lands.size();
    }

    public int getTroopsLeft() {
        return troopsLeft;
    }

    public ArrayList<String> getContinents() {
        return continents;
    }

    /** Setters **/
    public void setTroopsLeft(int troopsLeft) {
        this.troopsLeft = troopsLeft;
    }

    public void setPlayerToController(PlayerToController listener) {
        this.playerToController = listener;
    }

}
