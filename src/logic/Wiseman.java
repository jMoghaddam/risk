package logic;

import Models.WisemanToController;
import gui.gameboard.Land;
import helpers.SoundHolder;
import helpers.Utils;
import javax.sound.sampled.Clip;
import java.util.Collections;
import java.util.Random;

public class Wiseman {

    private final Player[] players;
    public CurrentState currentState = new CurrentState();
    private WisemanToController wisemanToController;
    private final Random random = new Random();
    private final Clip alert;
    private final Clip win;

    // Control the Logic of Game
    public Wiseman(int numberOfPlayers) {

        alert = Utils.createSound(SoundHolder.ALERT);
        win = Utils.createSound(SoundHolder.WIN);

        players = new Player[numberOfPlayers];

        int initialTroops = 0;
        switch (numberOfPlayers) {
            case 2:
                initialTroops = 30;
                break;

            case 3:
                initialTroops = 25;
                break;

            case 4:
                initialTroops = 20;
        }

        for (int i = 0; i < numberOfPlayers; i++) {
            players[i] = new Player(initialTroops);
        }

    }

    // On Land Clicked
    public void landClicked(Land clickedLand) {

        switch (CurrentState.state) {
            case "send initial troops":
                initialTroops(clickedLand);
                break;
            case "level1":
                sendTroopsAction(CurrentState.who, clickedLand);
                break;
            case "level2":
                if (CurrentState.attacker == null) {
                    if (clickedLand.getOwner() == CurrentState.who && clickedLand.getSoldiers() > 1) {
                        setAttackerLand(clickedLand);
                    } else alert();
                } else {
                    if (CurrentState.who != clickedLand.getOwner()) {
                        if (
                                clickedLand.id == CurrentState.attacker.id - 1 ||
                                        clickedLand.id == CurrentState.attacker.id + 1 ||
                                        clickedLand.id == CurrentState.attacker.id - 6 ||
                                        clickedLand.id == CurrentState.attacker.id + 6
                        ) {
                            setDefenderLand(clickedLand);
                            startWar();
                        }
                    } else alert();
                }
                break;
            case "level3":
                if (CurrentState.origin == null) {
                    if (clickedLand.getOwner() == CurrentState.who) {
                        CurrentState.origin = clickedLand;
                        clickedLand.becomeRed();
                    } else alert();
                } else {
                    if (clickedLand.getOwner() == CurrentState.who) {
                        CurrentState.destination = clickedLand;
                        if (checkConnectionPath(CurrentState.origin)) {
                            transfer();
                        } else alert();
                        clearSearchStatus();
                        CurrentState.origin.normalize();
                        CurrentState.origin = null;
                        CurrentState.destination = null;
                    } else alert();
                }
                break;
        }

    }

    // Set Initial Troops
    private void initialTroops(Land clickedLand) {
        if (clickedLand.getOwner() == currentState.who) {
            if (currentState.who.getTroopsLeft() != 0) {
                currentState.who.sendTroops(clickedLand, 1);
            }

            int nextId = (currentState.who.id == players.length) ? 1 : currentState.who.id + 1;

            if (players[nextId - 1].getTroopsLeft() == 0) {
                int counter = 0;
                boolean isSet = false;
                while (!isSet && counter < players.length) {
                    if (players[nextId - 1].getTroopsLeft() != 0) {
                        currentState.setState("send initial troops", players[nextId - 1]);
                        wisemanToController.updateTroopsBoard(players[nextId - 1], players[nextId - 1].getTroopsLeft());
                        wisemanToController.updateInfoBoard(players[nextId - 1]);
                        isSet = true;
                    } else {
                        nextId = (nextId == players.length) ? 1 : nextId + 1;
                        counter++;
                    }
                }

                if (!isSet) {
                    currentState.setState("level1", players[0]);
                    int troops = getTroops(players[0]);
                    players[0].setTroopsLeft(troops);
                    wisemanToController.updateTroopsBoard(players[0], troops);
                    wisemanToController.updateInfoBoard(players[0]);
                    wisemanToController.updateStateBoard(1);
                }
            } else {
                currentState.setState("send initial troops", players[nextId - 1]);
                wisemanToController.updateTroopsBoard(players[nextId - 1], players[nextId - 1].getTroopsLeft());
                wisemanToController.updateInfoBoard(players[nextId - 1]);
            }
        } else alert();
    }

    // Set Troops ( State 1 )
    private void sendTroopsAction(Player player, Land land) {
        if (land.getOwner() == player) {
            player.sendTroops(land, 1);

            if (player.getTroopsLeft() != 0) {
                currentState.setState("level1", player);
                wisemanToController.updateTroopsBoard(player, player.getTroopsLeft());
                wisemanToController.updateInfoBoard(player);
            } else {
                currentState.setState("level2", player);
                wisemanToController.wipeTroopsBoard();
                wisemanToController.updateInfoBoard(player);
                wisemanToController.updateStateBoard(2);
            }

        } else alert();
    }

    // Set Attacker Land ( State 2 )
    private void setAttackerLand(Land land) {
        CurrentState.attacker = land;

        wisemanToController.updateAttackMode(CurrentState.who, land);
    }

    // Set Defender Land ( State 2 )
    private void setDefenderLand(Land land) {
        CurrentState.defender = land;

        wisemanToController.allLandsToNormal();
    }

    // Cancel Attacker Land ( State 2 )
    public void clearAction() {

        if (CurrentState.state.equals("level2")) {
            CurrentState.attacker = null;
            wisemanToController.allLandsToNormal();
        }

    }

    // Start War ( State 2 )
    public void startWar() {

        int attackersNumber = 0;
        int defendersNumber = 0;

        if (CurrentState.attacker.getSoldiers() >= 4) attackersNumber = 3;
        else attackersNumber = CurrentState.attacker.getSoldiers() - 1;

        if (CurrentState.defender.getSoldiers() >= 2) defendersNumber = 2;
        else defendersNumber = 1;

        for (int i = 0; i < attackersNumber; i++) {
            int power = random.nextInt(6) + 1;
            CurrentState.attackers.add(power);
        }

        for (int i = 0; i < defendersNumber; i++) {
            int power = random.nextInt(6) + 1;
            CurrentState.defenders.add(power);
        }

        Collections.sort(CurrentState.attackers);
        Collections.reverse(CurrentState.attackers);
        Collections.sort(CurrentState.defenders);
        Collections.reverse(CurrentState.defenders);

        wisemanToController.updateDiceBoard();

        for (int i = 0; i < Math.min(CurrentState.attackers.size(), CurrentState.defenders.size()); i++) {

            if (CurrentState.attackers.get(i) > CurrentState.defenders.get(i)) {
                if (CurrentState.defender.getOwner() != CurrentState.attacker.getOwner()) {

                    if (CurrentState.defender.getSoldiers() > 1) {
                        CurrentState.defender.updateSoldiers(CurrentState.defender.getSoldiers() - 1);
                    } else {
                        CurrentState.defender.getOwner().looseLand(CurrentState.defender);
                        CurrentState.attacker.getOwner().winLand(CurrentState.defender);
                        CurrentState.defender.updateOwner(CurrentState.attacker.getOwner());
                        CurrentState.defender.updateSoldiers(1);
                        CurrentState.attacker.removeSoldier();
                        win();
                    }

                } else {
                    CurrentState.defender.addSoldier();
                    CurrentState.attacker.removeSoldier();
                }
            } else {
                CurrentState.attacker.removeSoldier();
            }

        }

        CurrentState.attacker = null;
        CurrentState.defender = null;
        CurrentState.attackers.clear();
        CurrentState.defenders.clear();

    }

    // Skip War ( State 2 )
    public void skipWar() {
        if (CurrentState.state.equals("level2")) {
            currentState.setState("level3", CurrentState.who);
            CurrentState.attacker = null;
            CurrentState.defender = null;
            CurrentState.attackers.clear();
            CurrentState.defenders.clear();
            wisemanToController.updateStateBoard(3);
            wisemanToController.wipeDiceBoard();
            wisemanToController.wipeTroopsBoard();
            wisemanToController.allLandsToNormal();
        } else {
            int nextId = (CurrentState.who.id == players.length) ? 1 : CurrentState.who.id + 1;
            currentState.setState("level1", players[nextId - 1]);
            CurrentState.origin = null;
            CurrentState.destination = null;
            int troops = getTroops(players[nextId - 1]);
            players[nextId - 1].setTroopsLeft(troops);
            wisemanToController.updateTroopsBoard(players[nextId - 1], troops);
            wisemanToController.updateInfoBoard(players[nextId - 1]);
            wisemanToController.updateStateBoard(1);
            wisemanToController.allLandsToNormal();
        }
    }

    // Transfer Troops ( State 3 )
    public void transfer() {
        if ( CurrentState.origin.getSoldiers() > 1 ) {
            CurrentState.origin.removeSoldier();
            CurrentState.destination.addSoldier();
        }
    }

    private boolean checkConnectionPath(Land land) {

        if (land == CurrentState.destination) return true;
        if (land.isSearched) return false;

        land.isSearched = true;

        boolean b1 = false, b2 = false, b3 = false, b4 = false;

        if (land.id - 1 >= 0 && (land.id - 1) % 6 != 0) {
            if (wisemanToController.getLand(land.id - 1).getOwner() == CurrentState.destination.getOwner()) {
                b1 = checkConnectionPath(wisemanToController.getLand(land.id - 1));
            }
        }

        if (land.id + 1 <= 42 && land.id % 6 != 0) {
            if (wisemanToController.getLand(land.id + 1).getOwner() == CurrentState.destination.getOwner()) {
                b2 = checkConnectionPath(wisemanToController.getLand(land.id + 1));
            }
        }

        if (land.id - 6 >= 0) {
            if (wisemanToController.getLand(land.id - 6).getOwner() == CurrentState.destination.getOwner()) {
                b3 = checkConnectionPath(wisemanToController.getLand(land.id - 6));
            }
        }

        if (land.id + 6 <= 42) {
            if (wisemanToController.getLand(land.id + 6).getOwner() == CurrentState.destination.getOwner()) {
                b4 = checkConnectionPath(wisemanToController.getLand(land.id + 6));
            }
        }

        return (b1 || b2 || b3 || b4);

    }

    private void clearSearchStatus() {

        for (int i = 0; i < 42; i++) {
            wisemanToController.getLand(i + 1).isSearched = false;
        }

    }

    /** Sound Effects **/
    private void alert() {
        alert.stop();
        alert.setMicrosecondPosition(0);
        alert.setFramePosition(0);
        alert.start();
    }

    private void win() {
        win.stop();
        win.setMicrosecondPosition(0);
        win.setFramePosition(0);
        win.start();
    }

    /** Getters **/
    public int getTroops(Player player) {
        int troops1 = player.getLandsNumber() / 3;
        int troops2 = 0;

        if (player.getContinents().contains("america")) troops2 += 3;
        if (player.getContinents().contains("europe")) troops2 += 4;
        if (player.getContinents().contains("asia")) troops2 += 4;
        if (player.getContinents().contains("africa")) troops2 += 2;

        return troops1 + troops2;

    }

    public Player getPlayer(int id) {
        return players[id - 1];
    }

    /** Setters **/
    public void setWisemanToController(WisemanToController listener) {
        this.wisemanToController = listener;
    }

}
